# Stack #
* Git
* Java 8
* Maven
* Spring Boot
* Spring Security
* Spring Data
* Hibernate
* H2
* Angular 2
* Bootstrap CSS + ng2-bootstrap
* Webpack

# How to start #
1. run ```start.sh``` from command line on Unix OSs from project's root or
2. run ```mvn clean package && java - jar target/simple-admin-panel-0.0.1-SNAPSHOT.jar``` on Windows from project's root or
3. build and run from your IDE, but first run ```webpack``` with nodejs or ```package``` with Maven

# Default users #
| Login                  | Password  | Role     |
|------------------------|-----------|----------|
|ivan.ivanov@gmail.com   |admin      |ADMIN     |
|petr.petrov@gmail.com   |operator   |ADOPS     |
|pavel.pavlov@gmail.com  |publisher  |PUBLISHER |
|oleg.olegov@gmail.com   |publisher  |PUBLISHER |