import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {User} from "../shared/User";

@Injectable()
export class UserService {

    constructor(private http: Http) {
    }

    getCurrentUser(): Promise<User> {
        return this.http.get('/user/current').toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }

    getUsers(): Promise<User[]> {
        return this.http.get('/user/list').toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }

    saveUser(user: User): Promise<User> {
        return this.http.post('/user', user).toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }

    deleteUser(id: number): Promise<Response> {
        return this.http.delete('/user/' + id).toPromise()
            .then(
                response => console.info(response.status)
            )
            .catch(
                error => console.error(error)
            );
    }

    getPublishers(): Promise<User[]> {
        return this.http.get('/user/publishers').toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }
}