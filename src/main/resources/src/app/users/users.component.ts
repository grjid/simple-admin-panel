import {Component, OnInit} from "@angular/core";
import {User} from "../shared/User";
import {UserService} from "./users.service";

@Component({
    selector: 'mc-users-settings',
    templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {

    isAdmin: boolean;

    currentUser: User;

    users: User[];

    constructor(private usersService: UserService) {
        this.isAdmin = false;
        this.currentUser = new User();
        this.users = [];
    }

    editUser() {
        this.usersService.saveUser(this.currentUser).then(
            () => this.usersService.getUsers().then(
                users => this.users = users
            )
        )
    }

    assignUser(user: User) {
        this.currentUser = user;
    }

    deleteUser() {
        if (this.currentUser.id) {
            this.usersService.deleteUser(this.currentUser.id).then(
                () => this.usersService.getUsers().then(
                    users => {
                        this.users = users;
                        this.resetCurrentUser();
                    }
                )
            )
        }
    }

    resetCurrentUser() {
        this.currentUser = new User();
    }

    ngOnInit() {
        this.usersService.getCurrentUser().then(
            user => {
                this.isAdmin = user.role === 'ADMIN';
            }
        );

        this.usersService.getUsers().then(
            users => this.users = users
        );
    }
}