import {Component, OnInit} from "@angular/core";
import {Application} from "../shared/App";
import {AppService} from "./apps.service";
import {UserService} from "../users/users.service";
import {User} from "../shared/User";

@Component({
    selector: 'mc-apps-settings',
    templateUrl: './apps.component.html'
})
export class AppsComponent implements OnInit {

    isAdmin: boolean;

    authorizedUser: User;

    currentApp: Application;

    apps: Application[];

    availableContentTypes: string[];

    contentTypes: string[];

    publishers: User[];

    constructor(private appService: AppService,
                private userService: UserService) {
        this.isAdmin = false;
        this.authorizedUser = new User();
        this.currentApp = new Application();
        this.apps = [];
        this.contentTypes = ['VIDEO', 'IMAGE', 'HTML'];
        this.availableContentTypes = ['VIDEO', 'IMAGE', 'HTML'];
        this.publishers = [];
    }

    selectApp(app: Application) {
        this.currentApp = app;
        this.availableContentTypes = [];
        for (let ct of this.contentTypes) {
            if (this.currentApp.contentTypes.indexOf(ct) < 0) {
                this.availableContentTypes.push(ct);
            }
        }
    }

    editApp() {
        this.appService.saveApp(this.currentApp).then(
            () => this.appService.getApps().then(
                apps => this.apps = apps
            )
        )
    }

    deleteApp() {
        if (this.currentApp.id) {
            this.appService.deleteApp(this.currentApp.id).then(
                () => {
                    this.appService.getApps().then(
                        apps => {
                            this.apps = apps;
                            this.resetCurrentApp();
                        }
                    )
                }
            )
        }
    }

    resetCurrentApp() {
        this.currentApp = new Application();
        this.availableContentTypes = ['VIDEO', 'IMAGE', 'HTML'];
    }

    getPublishers() {
        if (!this.currentApp.id && this.authorizedUser.role !== 'PUBLISHER') {
            this.userService.getPublishers().then(
                publishers => this.publishers = publishers
            )
        } else {
            this.currentApp.publisherId = this.authorizedUser.id;
        }
    }

    ngOnInit() {
        this.userService.getCurrentUser().then(
            user => {
                this.authorizedUser = user;
                this.isAdmin = user.role === 'ADMIN';
            }
        );

        this.appService.getApps().then(
            apps =>
                this.apps = apps
        );
    }
}