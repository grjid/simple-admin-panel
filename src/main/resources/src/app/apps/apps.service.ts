import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {Application} from "../shared/App";

@Injectable()
export class AppService {

    constructor(private http: Http) {
    }

    getApps(): Promise<Application[]> {
        return this.http.get('/app/list').toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }

    saveApp(app: Application): Promise<Application> {
        return this.http.post('/app', app).toPromise()
            .then(
                response => response.json()
            )
            .catch(
                error => console.error(error)
            )
    }

    deleteApp(id: number): Promise<Response> {
        return this.http.delete('/app/' + id).toPromise()
            .then(
                response => console.info(response.status)
            )
            .catch(
                error => console.error(error)
            );
    }
}