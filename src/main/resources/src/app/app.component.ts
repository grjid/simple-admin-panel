import {Component, OnInit} from "@angular/core";
import "../style.css";
import {UserService} from "./users/users.service";

@Component({
    selector: 'mc-app',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    isAdmin: boolean;

    constructor(private userService: UserService) {
        this.isAdmin = false;
    }

    ngOnInit(): void {
        this.userService.getCurrentUser().then(
            user => {
                this.isAdmin = user.role !== 'PUBLISHER';
            }
        )
    }

}