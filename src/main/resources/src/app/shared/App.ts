export class Application {
    id: number;
    name: string;
    type: string;
    contentTypes: string[];
    publisherId: number;
    publisherName: string;
}