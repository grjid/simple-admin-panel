import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {TabsModule, ModalModule, SortableModule} from "ng2-bootstrap";
import {AppsComponent} from "./apps/apps.component";
import {UsersComponent} from "./users/users.component";
import {AppService} from "./apps/apps.service";
import {HttpModule} from "@angular/http";
import {UserService} from "./users/users.service";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        TabsModule.forRoot(),
        ModalModule.forRoot(),
        SortableModule.forRoot()
    ],
    declarations: [
        AppComponent,
        AppsComponent,
        UsersComponent
    ],
    providers: [
        AppService,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}