package org.bitbucket.grjid.controller;

import org.bitbucket.grjid.domain.web.UserModel;
import org.bitbucket.grjid.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public ResponseEntity findAllUsers() {
        return new ResponseEntity<>(adminService.findAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/publishers", method = RequestMethod.GET)
    public ResponseEntity findPublishers() {
        return new ResponseEntity<>(adminService.findPublishers(), HttpStatus.OK);
    }


    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity saveUser(@RequestBody UserModel user) {
        return new ResponseEntity<>(adminService.saveUser(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/current", method = RequestMethod.GET)
    public ResponseEntity getCurrentUser() {
        return new ResponseEntity<>(adminService.getCurrentUser(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity findUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(adminService.findUserById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable("id") Long id) {
        adminService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
