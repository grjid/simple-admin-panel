package org.bitbucket.grjid.controller;

import org.bitbucket.grjid.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getMain() {
        String username = adminService.getCurrentUser().getName();
        ModelAndView model = new ModelAndView("main");
        model.addObject("username", username);
        return model;
    }

}
