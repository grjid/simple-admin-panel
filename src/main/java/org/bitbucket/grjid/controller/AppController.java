package org.bitbucket.grjid.controller;

import org.bitbucket.grjid.domain.web.AppModel;
import org.bitbucket.grjid.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AppController {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/app/list", method = RequestMethod.GET)
    public ResponseEntity findAllApps() {
        return new ResponseEntity<>(adminService.findAllApps(), HttpStatus.OK);
    }

    @RequestMapping(value = "/app/{id}", method = RequestMethod.GET)
    public ResponseEntity findApp(@PathVariable("id") Long id) {
        return new ResponseEntity<>(adminService.findApp(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/app", method = RequestMethod.POST)
    public ResponseEntity saveApp(@RequestBody AppModel request) {
        return new ResponseEntity<>(adminService.createApp(request), HttpStatus.OK);
    }

    @RequestMapping(value = "/app/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteApp(@PathVariable("id") Long id) {
        adminService.deleteApp(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
