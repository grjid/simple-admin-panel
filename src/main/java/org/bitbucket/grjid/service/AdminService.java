package org.bitbucket.grjid.service;

import org.bitbucket.grjid.domain.web.AppModel;
import org.bitbucket.grjid.domain.web.UserModel;

import java.util.List;

public interface AdminService {

    UserModel getCurrentUser();

    UserModel saveUser(UserModel user);

    void deleteUser(Long userId);

    UserModel findUserById(Long userId);

    List<UserModel> findAllUsers();

    List<UserModel> findPublishers();

    AppModel createApp(AppModel request);

    void deleteApp(Long appId);

    AppModel findApp(Long appId);

    List<AppModel> findAllApps();

}
