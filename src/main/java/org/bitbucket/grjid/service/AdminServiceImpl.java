package org.bitbucket.grjid.service;

import org.bitbucket.grjid.NoDataException;
import org.bitbucket.grjid.domain.UserRole;
import org.bitbucket.grjid.domain.entities.App;
import org.bitbucket.grjid.domain.entities.User;
import org.bitbucket.grjid.domain.web.AppModel;
import org.bitbucket.grjid.domain.web.UserModel;
import org.bitbucket.grjid.repository.AppRepository;
import org.bitbucket.grjid.repository.UserRepository;
import org.bitbucket.grjid.service.security.UserDetailsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private UserDetailsComponent userDetails;

    @Override
    public UserModel saveUser(UserModel userModel) {
        if (Objects.isNull(userModel.getId())) {
            return new UserModel(
                    userRepository.save(
                            createUser(userModel)));
        }
        return new UserModel(userRepository.save(updateUser(userModel)));
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.delete(userRepository.findOne(userId));
    }

    @Override
    public UserModel findUserById(Long userId) {
        return new UserModel(userRepository.findOne(userId));
    }

    @Override
    public List<UserModel> findAllUsers() {
        List<UserModel> result = new ArrayList<>();
        userRepository.findAll().forEach(
                user -> result.add(new UserModel(user)));
        return result;
    }

    @Override
    public List<UserModel> findPublishers() {
        return userRepository.findByRole(UserRole.PUBLISHER).stream()
                .map(UserModel::new)
                .collect(Collectors.toList());
    }

    @Override
    public AppModel createApp(AppModel model) {
        User user = userRepository.findOne(model.getPublisherId());
        if (user != null) {
            App app = appRepository.save(new App(model, user));
            user.addApp(app);
            userRepository.save(user);
            return new AppModel(app);
        }
        throw new NoDataException("User with such ID does not exist!");
    }

    @Override
    public void deleteApp(Long appId) {
        appRepository.delete(appRepository.findOne(appId));
    }

    @Override
    public AppModel findApp(Long appId) {
        return findAllApps().stream()
                .filter(app -> appId.equals(app.getId())).findFirst()
                .orElseThrow(NoDataException::new);
    }

    @Override
    public List<AppModel> findAllApps() {
        User currentUser = userRepository.findOne(getCurrentUser().getId());

        if (UserRole.PUBLISHER.equals(currentUser.getRole())) {
            return currentUser.getApps()
                    .stream()
                    .map(AppModel::new)
                    .collect(toList());
        }
        return StreamSupport.stream(appRepository.findAll().spliterator(), false)
                .map(AppModel::new)
                .collect(toList());
    }

    public UserModel getCurrentUser() {
        return new UserModel(userRepository.findByEmail(userDetails.getAuthenticatedUser()));
    }

    private User updateUser(UserModel userModel) {
        User user = userRepository.findOne(userModel.getId());
        if (Objects.nonNull(userModel.getName()) && !user.getName().equals(userModel.getName())) {
            user.setName(userModel.getName());
        }
        if (Objects.nonNull(userModel.getEmail()) && !user.getEmail().equals(userModel.getEmail())) {
            user.setEmail(userModel.getEmail());
        }
        if (Objects.nonNull(userModel.getRole()) && !user.getRole().equals(userModel.getRole())) {
            user.setRole(userModel.getRole());
        }
        return user;
    }

    private User createUser(UserModel userModel) {
        User user = new User(userModel);
        if (user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return user;
    }

}
