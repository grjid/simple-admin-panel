package org.bitbucket.grjid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleAdminPanelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleAdminPanelApplication.class, args);
	}
}
