package org.bitbucket.grjid.repository;

import org.bitbucket.grjid.domain.entities.App;
import org.springframework.data.repository.CrudRepository;

public interface AppRepository extends CrudRepository<App, Long> {
}
