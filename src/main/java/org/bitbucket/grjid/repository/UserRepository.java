package org.bitbucket.grjid.repository;

import org.bitbucket.grjid.domain.UserRole;
import org.bitbucket.grjid.domain.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);

    List<User> findByRole(UserRole role);
}
