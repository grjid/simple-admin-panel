package org.bitbucket.grjid.support;

import org.bitbucket.grjid.domain.*;
import org.bitbucket.grjid.domain.entities.App;
import org.bitbucket.grjid.domain.entities.User;
import org.bitbucket.grjid.repository.AppRepository;
import org.bitbucket.grjid.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DatabaseInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseInitializer.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AppRepository appRepository;

    @PostConstruct
    public void fillDatabase() {
        User admin = new User("Ivan Ivanov", "ivan.ivanov@gmail.com", "admin", UserRole.ADMIN);
        User operator = new User("Petr Petrov", "petr.petrov@gmail.com", "operator", UserRole.ADOPS);
        User publisher1 = new User("Pavel Pavlov", "pavel.pavlov@gmail.com", "publisher", UserRole.PUBLISHER);
        User publisher2 = new User("Oleg Olegov", "oleg.olegov@gmail.com", "publisher", UserRole.PUBLISHER);

        userRepository.save(admin);
        userRepository.save(operator);
        userRepository.save(publisher1);
        userRepository.save(publisher2);

        App app1 = new App("Awesome app", AppType.ANDROID, publisher1, ContentType.IMAGE, ContentType.VIDEO);
        App app2 = new App("Incredible app", AppType.IOS, publisher1, ContentType.IMAGE, ContentType.VIDEO);
        App app3 = new App("Fabulous app", AppType.WEBSITE, publisher1, ContentType.IMAGE, ContentType.VIDEO, ContentType.HTML);
        App app4 = new App("Test app", AppType.WEBSITE, publisher2, ContentType.IMAGE, ContentType.HTML);

        appRepository.save(app1);
        appRepository.save(app2);
        appRepository.save(app3);
        appRepository.save(app4);

        publisher1.addApp(app1, app2, app3);
        publisher2.addApp(app4);

        userRepository.save(publisher1);
        userRepository.save(publisher2);

        LOGGER.info("[DatabaseInitializer::fillDatabase] Default users created: {}, {}, {}, {}",
                admin, operator, publisher1, publisher2);
    }

}
