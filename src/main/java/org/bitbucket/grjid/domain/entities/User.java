package org.bitbucket.grjid.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bitbucket.grjid.domain.UserRole;
import org.bitbucket.grjid.domain.web.UserModel;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column(unique = true)
    @Email
    private String email;

    @Column
    @JsonIgnore
    private String password;

    @Column
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<App> apps = new HashSet<>();

    public User() {
    }

    public User(UserModel userModel) {
        this.name = userModel.getName();
        this.email = userModel.getEmail();
        this.role = userModel.getRole();
        if (Objects.nonNull(userModel.getId())) {
            this.id = userModel.getId();
        }
        if (Objects.nonNull(userModel.getPassword())) {
            this.password = userModel.getPassword();
        }
    }

    public User(String name, String email, String password, UserRole role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Set<App> getApps() {
        return apps;
    }

    public void setApps(Set<App> apps) {
        this.apps = apps;
    }

    public void addApp(App... apps) {
        Collections.addAll(this.apps, apps);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                '}';
    }
}
