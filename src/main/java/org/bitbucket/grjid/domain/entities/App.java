package org.bitbucket.grjid.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bitbucket.grjid.domain.AppType;
import org.bitbucket.grjid.domain.ContentType;
import org.bitbucket.grjid.domain.web.AppModel;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class App {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    @Enumerated(EnumType.STRING)
    private AppType type;

    @ElementCollection(targetClass = ContentType.class)
    @Column
    @Enumerated(EnumType.STRING)
    private Set<ContentType> contentTypes = new HashSet<>();

    @ManyToOne
    @JsonIgnore
    private User user;

    public App() {
    }

    public App(String name, AppType type, Set<ContentType> contentTypes, User user) {
        this.name = name;
        this.type = type;
        this.contentTypes = contentTypes;
        this.user = user;
    }

    public App(String name, AppType type, User user, ContentType... contentTypes) {
        this.name = name;
        this.type = type;
        this.user = user;
        Collections.addAll(this.contentTypes, contentTypes);
    }

    public App(AppModel appModel, User user) {
        this.name = appModel.getName();
        this.type = appModel.getType();
        this.contentTypes = appModel.getContentTypes();
        this.user = user;
        if (Objects.nonNull(appModel.getId())) {
            this.id = appModel.getId();
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public App setName(String name) {
        this.name = name;
        return this;
    }

    public AppType getType() {
        return type;
    }

    public App setType(AppType type) {
        this.type = type;
        return this;
    }

    public Set<ContentType> getContentTypes() {
        return contentTypes;
    }

    public App setContentTypes(Set<ContentType> contentTypes) {
        this.contentTypes = contentTypes;
        return this;
    }

    public User getUser() {
        return user;
    }

    public App setUser(User user) {
        this.user = user;
        return this;
    }
}
