package org.bitbucket.grjid.domain;

public enum UserRole {

    ADMIN, ADOPS, PUBLISHER
}
