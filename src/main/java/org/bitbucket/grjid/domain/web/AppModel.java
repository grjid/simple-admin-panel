package org.bitbucket.grjid.domain.web;

import org.bitbucket.grjid.domain.AppType;
import org.bitbucket.grjid.domain.ContentType;
import org.bitbucket.grjid.domain.entities.App;

import java.util.HashSet;
import java.util.Set;

public class AppModel {

    private Long id;

    private String name;

    private AppType type;

    private Set<ContentType> contentTypes = new HashSet<>();

    private Long publisherId;

    private String publisherName;

    public AppModel() {
    }

    public AppModel(App app) {
        this.id = app.getId();
        this.name = app.getName();
        this.type = app.getType();
        this.contentTypes = app.getContentTypes();
        this.publisherId = app.getUser().getId();
        this.publisherName = app.getUser().getName();
    }

    public Long getId() {
        return id;
    }

    public AppModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AppModel setName(String name) {
        this.name = name;
        return this;
    }

    public AppType getType() {
        return type;
    }

    public AppModel setType(AppType type) {
        this.type = type;
        return this;
    }

    public Set<ContentType> getContentTypes() {
        return contentTypes;
    }

    public AppModel setContentTypes(Set<ContentType> contentTypes) {
        this.contentTypes = contentTypes;
        return this;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public AppModel setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
        return this;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public AppModel setPublisherName(String publisherName) {
        this.publisherName = publisherName;
        return this;
    }
}
