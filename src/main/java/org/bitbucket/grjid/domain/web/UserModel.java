package org.bitbucket.grjid.domain.web;

import org.bitbucket.grjid.domain.UserRole;
import org.bitbucket.grjid.domain.entities.User;

public class UserModel {

    private Long id;

    private String name;

    private String email;

    private String password;

    private UserRole role;

    public UserModel() {
    }

    public UserModel(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.role = user.getRole();
    }

    public Long getId() {
        return id;
    }

    public UserModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserModel setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserRole getRole() {
        return role;
    }

    public UserModel setRole(UserRole role) {
        this.role = role;
        return this;
    }

}
