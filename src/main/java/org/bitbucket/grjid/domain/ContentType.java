package org.bitbucket.grjid.domain;

public enum ContentType {

    VIDEO, IMAGE, HTML

}
