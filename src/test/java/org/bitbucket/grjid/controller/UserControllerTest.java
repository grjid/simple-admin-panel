package org.bitbucket.grjid.controller;

import org.bitbucket.grjid.domain.web.UserModel;
import org.bitbucket.grjid.service.AdminServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdminServiceImpl adminService;

    @Test
    public void methodShouldReturnStatus401ForUnauthorizedUsers() throws Exception {
        when(adminService.findUserById(anyLong())).thenReturn(new UserModel());
        mockMvc
                .perform(MockMvcRequestBuilders.get("/user/1"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
        verify(adminService, times(0)).findUserById(1L);
    }

    @Test
    @WithMockUser(roles = {"PUBLISHER"})
    public void methodShouldReturnStatus403ForPublisherRole() throws Exception {
        when(adminService.findUserById(anyLong())).thenReturn(new UserModel());
        mockMvc
                .perform(MockMvcRequestBuilders.get("/user/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
        verify(adminService, times(0)).findUserById(1L);
    }

    @Test
    @WithMockUser
    public void methodShouldReturnStatus200() throws Exception {
        when(adminService.findUserById(anyLong())).thenReturn(new UserModel());
        mockMvc
                .perform(MockMvcRequestBuilders.get("/user/1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        verify(adminService, times(1)).findUserById(1L);
    }

}