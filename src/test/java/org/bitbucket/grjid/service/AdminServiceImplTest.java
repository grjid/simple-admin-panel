package org.bitbucket.grjid.service;

import org.bitbucket.grjid.NoDataException;
import org.bitbucket.grjid.domain.AppType;
import org.bitbucket.grjid.domain.entities.App;
import org.bitbucket.grjid.domain.entities.User;
import org.bitbucket.grjid.domain.web.AppModel;
import org.bitbucket.grjid.repository.AppRepository;
import org.bitbucket.grjid.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;

@RunWith(SpringRunner.class)
public class AdminServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AppRepository appRepository;

    @InjectMocks
    private AdminServiceImpl adminService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void userShouldBeSavedWhenAppUpdated() {
        AppModel appModelStub = new AppModel()
                .setName("Test")
                .setPublisherId(31L)
                .setType(AppType.WEBSITE);
        Mockito.when(userRepository.findOne(anyLong())).thenReturn(new User());
        Mockito.when(appRepository.save(any(App.class))).thenReturn(new App().setUser(new User()));
        adminService.createApp(appModelStub);
        Mockito.verify(userRepository, Mockito.times(1)).save(any(User.class));
    }

    @Test(expected = NoDataException.class)
    public void noDataExceptionShouldBeThrownWhenUserNotFoundWhileSavingAnApp() {
        Mockito.when(userRepository.findOne(anyLong())).thenReturn(null);
        adminService.createApp(new AppModel());
    }


}